import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    titles = ["Mr.", "Mrs.", "Miss."]
    res = []

    for title in titles:
        filtered_bytitle = df[df.Name.str.contains(title + ' ')]
        missing_values = filtered_bytitle.Age.isnull().sum()
        median_value = round(filtered_bytitle.Age.median())
        res.append((title, missing_values, median_value))
    
    return res
